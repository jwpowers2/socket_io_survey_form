
var express = require( "express");
var path = require( "path");

var app = express();

app.use(express.static(path.join(__dirname, "./static")));

app.set('views', path.join(__dirname, './views'));
app.set('view engine', 'ejs');

app.get("/", function (req, res){

    res.render('index');

})

var server = app.listen(8000, function() {
 console.log("listening on port 8000");
});

// pass server object to socket

var io = require('socket.io').listen(server);

// io object to control socket

io.sockets.on('connection', function(socket){
	console.log("Client/socket is connected");
	console.log("Client/socket id is: ", socket.id);

    socket.on( "posting_form", function (data){

      //console.log(data);
      socket.emit( 'updated_message', data);
      let random_number = Math.floor(Math.random() * 10); 
      socket.emit('random_number',random_number);


    });
    
})
